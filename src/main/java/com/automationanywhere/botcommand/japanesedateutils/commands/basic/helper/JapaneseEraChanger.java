package com.automationanywhere.botcommand.japanesedateutils.commands.basic.helper;

public class JapaneseEraChanger {

    private static String[] fullNameEra = {"明治", "大正", "昭和", "平成", "令和"};
    private static String[] singleNameEra = {"明", "大", "昭", "平", "令"};
    private static String[] singlCombinedNameEra = {"㍾", "㍽", "㍼", "㍻", "㋿"};
    private static String[] singleNumberNameEra = {"1", "2", "3", "4", "5"};
    private static String[] singleAlphabetNameEra = {"M", "T", "S", "H", "R"};


    public static String FullToSingle(String source){
        if(source.isEmpty())
            return source;

        String result =source;
        for (int num = 0; num < 5; num++) {
            result = result.replace(fullNameEra[num],singleNameEra[num]);
        }
        return result;
    }

    public static String SingleToFull(String source){
        if(source.isEmpty())
            return source;

        String result =source;

        for (int num = 0; num < 5; num++) {
            result = result.replace(singleNameEra[num],fullNameEra[num]);
        }
        return result;
    }

    public static String FullToSingleCombined(String source){
        if(source.isEmpty())
            return source;

        String result =source;

        for (int num = 0; num < 5; num++) {
            result = result.replace(fullNameEra[num],singlCombinedNameEra[num]);
        }
        return result;
    }

    public static String SingleCombinedToFull(String source){
        if(source.isEmpty())
            return source;

        String result =source;

        for (int num = 0; num < 5; num++) {
            result = result.replace(singlCombinedNameEra[num],fullNameEra[num]);
        }
        return result;
    }

    public static String FullToNumberSingle(String source){
        if(source.isEmpty())
            return source;

        String result =source;

        for (int num = 0; num < 5; num++) {
            result = result.replace(fullNameEra[num],singleNumberNameEra[num]);
        }
        return result;
    }

    public static String NumberSingleToFull(String source){
        if(source.isEmpty())
            return source;

        String result =source;

        for (int num = 0; num < 5; num++) {
            // replace only first character
            if(source.substring(0,1).matches(singleNumberNameEra[num])){
                result = fullNameEra[num] + source.substring(1,result.length());
               break;
            }
        }
        return result;
    }
}
