package com.automationanywhere.botcommand.japanesedateutils.commands.basic;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DateTimeValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

import static com.automationanywhere.commandsdk.model.DataType.DATETIME;

/**
 * Get holiday name.
 * Accepts one date inputs.
 * The date string must be Not NULL and providing the output variable is also mandatory.
 *
 * @author Yuji Hirose
 */

@BotCommand(commandType = BotCommand.CommandType.Command)

@CommandPkg(
        //Unique name inside a package and label to display.
        name = "GetPreviousNextDayOfWeek", label = "[[GetPreviousNextDayOfWeek.label]]",
        node_label = "[[GetPreviousNextDayOfWeek.node_label]]", description = "[[GetPreviousNextDayOfWeek.description]]", icon = "DateIcon.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[GetPreviousNextDayOfWeek.return_label]]", return_type = DATETIME, return_required = true)
public class GetPreviousNextDayOfWeek {

    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.japanesedateutils.messages");

    @Execute
    public Value<ZonedDateTime> action(
            @Idx(index = "1", type = AttributeType.DATETIME)
            @Pkg(label = "[[GetPreviousNextDayOfWeek.targetDate.label]]")
            @NotEmpty
                    ZonedDateTime targetDate,
            @Idx(index = "2", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "2.1", pkg = @Pkg(label = "[[GetPreviousNextDayOfWeek.type.2.1.label]]", value = "Sunday")),
                    @Idx.Option(index = "2.2", pkg = @Pkg(label = "[[GetPreviousNextDayOfWeek.type.2.2.label]]", value = "Monday")),
                    @Idx.Option(index = "2.3", pkg = @Pkg(label = "[[GetPreviousNextDayOfWeek.type.2.3.label]]", value = "Tuesday")),
                    @Idx.Option(index = "2.4", pkg = @Pkg(label = "[[GetPreviousNextDayOfWeek.type.2.4.label]]", value = "Wednesday")),
                    @Idx.Option(index = "2.5", pkg = @Pkg(label = "[[GetPreviousNextDayOfWeek.type.2.5.label]]", value = "Thursday")),
                    @Idx.Option(index = "2.6", pkg = @Pkg(label = "[[GetPreviousNextDayOfWeek.type.2.6.label]]", value = "Friday")),
                    @Idx.Option(index = "2.7", pkg = @Pkg(label = "[[GetPreviousNextDayOfWeek.type.2.7.label]]", value = "Saturday"))})
            @Pkg(label = "[[GetPreviousNextDayOfWeek.type.label]]")
            @NotEmpty
                    String targetDayOfWeek,
            @Idx(index = "3", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "3.1", pkg = @Pkg(label = "[[GetPreviousNextDayOfWeek.type.3.1.label]]", value = "Last")),
                    @Idx.Option(index = "3.2", pkg = @Pkg(label = "[[GetPreviousNextDayOfWeek.type.3.2.label]]", value = "Next"))
            })
            @Pkg(label = "[[GetPreviousNextDayOfWeek.beforeAfter.label]]")
            @NotEmpty
                    String beforeAfter
    ) {

        // valideate for null user-defined variable
        if (null == targetDate)
            throw new BotCommandException(MESSAGES.getString("emptyInputDate", "targetDate"));
        if (null == targetDayOfWeek)
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "type"));
        if (null == beforeAfter)
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "beforeAfter"));

        //Business logic
        ZonedDateTime result;
        boolean equalsTargetDayOfWeek = false;

        Instant instant = targetDate.toInstant();
        Date date = Date.from(instant);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        int dayOfWeek;

        do {
            if ("Next".equals(beforeAfter)) {
                calendar.add(Calendar.DAY_OF_YEAR, 1);
            } else if ("Last".equals(beforeAfter)) {
                calendar.add(Calendar.DAY_OF_YEAR, -1);
            } else {
                throw new BotCommandException(MESSAGES.getString("invalidType", "beforeAfter"));
            }
            dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

            switch (targetDayOfWeek) {
                case "Sunday":
                    equalsTargetDayOfWeek = Calendar.SUNDAY == dayOfWeek;
                    break;
                case "Monday":
                    equalsTargetDayOfWeek = Calendar.MONDAY == dayOfWeek;
                    break;
                case "Tuesday":
                    equalsTargetDayOfWeek = Calendar.TUESDAY == dayOfWeek;
                    break;
                case "Wednesday":
                    equalsTargetDayOfWeek = Calendar.WEDNESDAY == dayOfWeek;
                    break;
                case "Thursday":
                    equalsTargetDayOfWeek = Calendar.THURSDAY == dayOfWeek;
                    break;
                case "Friday":
                    equalsTargetDayOfWeek = Calendar.FRIDAY == dayOfWeek;
                    break;
                case "Saturday":
                    equalsTargetDayOfWeek = Calendar.SATURDAY == dayOfWeek;
                    break;
                default:
                    throw new BotCommandException(MESSAGES.getString("invalidType", "type"));
            }
        } while (!equalsTargetDayOfWeek);

        instant = calendar.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        result = ZonedDateTime.ofInstant(instant, zone);

        return new DateTimeValue(result);
    }

}
