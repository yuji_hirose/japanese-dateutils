package com.automationanywhere.botcommand.japanesedateutils.commands.basic;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DateTimeValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.botcommand.japanesedateutils.commands.basic.helper.JapaneseHolidayUtils;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

import static com.automationanywhere.commandsdk.model.DataType.DATETIME;

/**
 *
 * Get holiday name.
 * Accepts one date inputs.
 * The date string must be Not NULL and providing the output variable is also mandatory.
 *
 * @author Yuji Hirose
 */

@BotCommand(commandType = BotCommand.CommandType.Command)

@CommandPkg(
        //Unique name inside a package and label to display.
        name = "GetFirstLastDateOfMonth", label = "[[GetFirstLastDateOfMonth.label]]",
        node_label = "[[GetFirstLastDateOfMonth.node_label]]", description = "[[GetFirstLastDateOfMonth.description]]", icon = "DateIcon.svg",

        //Return type information. return_type ensures only the right kind of variable is provided on the UI.
        return_label = "[[GetFirstLastDateOfMonth.return_label]]", return_type = DATETIME, return_required = true)
public class GetFirstLastDateOfMonth {

    private static final Messages MESSAGES = MessagesFactory
            .getMessages("com.automationanywhere.botcommand.japanesedateutils.messages");

    @Execute
    public Value<ZonedDateTime> action(
            @Idx(index = "1", type = AttributeType.DATETIME)
            @Pkg(label = "[[GetFirstLastDateOfMonth.targetDate.label]]")
            @NotEmpty
                    ZonedDateTime targetDate,
            @Idx(index = "2", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "2.1", pkg = @Pkg(label = "[[GetFirstLastDateOfMonth.type.2.1.label]]", value = "FirstOfMonth")),
                    @Idx.Option(index = "2.2", pkg = @Pkg(label = "[[GetFirstLastDateOfMonth.type.2.2.label]]", value = "LastOfMonth"))})
            @Pkg(label = "[[GetFirstLastDateOfMonth.type.label]]")
            @NotEmpty
                    String type,
            @Idx(index = "3", type = AttributeType.RADIO, options = {
                    @Idx.Option(index = "3.1", pkg = @Pkg(label = "[[GetFirstLastDateOfMonth.type.3.1.label]]", value = "Last")),
                    @Idx.Option(index = "3.2", pkg = @Pkg(label = "[[GetFirstLastDateOfMonth.type.3.2.label]]", value = "This")),
                    @Idx.Option(index = "3.3", pkg = @Pkg(label = "[[GetFirstLastDateOfMonth.type.3.3.label]]", value = "Next"))
            })
            @Pkg(label = "[[GetFirstLastDateOfMonth.beforeAfter.label]]")
            @NotEmpty
                    String beforeAfter
    ){

        // valideate for null user-defined variable
        if(null==targetDate)
            throw new BotCommandException(MESSAGES.getString("emptyInputDate", "targetDate"));
        if(null==type)
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "type"));
        if(null==beforeAfter)
            throw new BotCommandException(MESSAGES.getString("emptyInputString", "beforeAfter"));

        //Business logic
        ZonedDateTime result;

        Instant instant = targetDate.toInstant();
        Date date = Date.from(instant);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.clear(Calendar.MINUTE);
        calendar.clear(Calendar.SECOND);
        calendar.clear(Calendar.MILLISECOND);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        if("Last".equals(beforeAfter))
            calendar.add(Calendar.MONTH, -1);
        if("Next".equals(beforeAfter))
            calendar.add(Calendar.MONTH, 1);

        if("LastOfMonth".equals(type))
            calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        instant = calendar.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        result = ZonedDateTime.ofInstant(instant, zone);

        return new DateTimeValue(result);
    }

}
