package com.automationanywhere.botcommand.japanesedateutils.commands.basic;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.japanesedateutils.commands.basic.helper.JapaneseEraChanger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.ZoneId;
import java.time.ZonedDateTime;

public class JapaneseEraChangerTest {
    @Test
    public void JapaneseEraChangerTestWithOption1(){

        String inputString = "令和2年5月1日";
        String expectedOutput = "令2年5月1日";

        String result = JapaneseEraChanger.FullToSingle(inputString);

        Assert.assertEquals(result, expectedOutput);
    }

    @Test
    public void JapaneseEraChangerTestWithOption2(){

        String inputString = "平成2年5月1日";
        String expectedOutput = "平2年5月1日";

        String result = JapaneseEraChanger.FullToSingle(inputString);

        Assert.assertEquals(result, expectedOutput);
    }

    @Test
    public void JapaneseEraChangerTestWithOption3(){

        String inputString = "令2年5月1日";
        String expectedOutput = "令和2年5月1日";

        String result = JapaneseEraChanger.SingleToFull(inputString);

        Assert.assertEquals(result, expectedOutput);
    }

    @Test
    public void JapaneseEraChangerTestWithOption4(){

        String inputString = "平2年5月1日";
        String expectedOutput = "平成2年5月1日";

        String result = JapaneseEraChanger.SingleToFull(inputString);

        Assert.assertEquals(result, expectedOutput);
    }

    @Test
    public void JapaneseEraChangerTestWithOption5(){

        String inputString = "令和2年5月1日";
        String expectedOutput = "52年5月1日";

        String result = JapaneseEraChanger.FullToNumberSingle(inputString);

        Assert.assertEquals(result, expectedOutput);
    }

    @Test
    public void JapaneseEraChangerTestWithOption6(){

        String inputString = "平成2年5月1日";
        String expectedOutput = "42年5月1日";

        String result = JapaneseEraChanger.FullToNumberSingle(inputString);

        Assert.assertEquals(result, expectedOutput);
    }

    @Test
    public void JapaneseEraChangerTestWithOption7(){

        String inputString = "5020501";
        String expectedOutput = "令和020501";

        String result = JapaneseEraChanger.NumberSingleToFull(inputString);

        Assert.assertEquals(result, expectedOutput);
    }
}
